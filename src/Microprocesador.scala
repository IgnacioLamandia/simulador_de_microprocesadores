package Instruccion;
package Nop;
package Add;
package Sub;
package Mul;
package Div;
package Swap;
package Halt;
package LodV;
package Lod;
package Str;

class Microprocesador {
  var memoria = new Array[Byte] (1024)
  private var a:Int = 0
  private var b:Int = 0

  def ejecutarInstruccion(instruccion:Instruccion){
    instruccion match { 
    case Nop() => 0;
    case Add() => a+b;
    case Sub() => a-b;
    case Mul() => a*b;
    case Div() => a/b
    case Swap() => val temp = a
                    a = b
                    b = temp
    
    case Halt() => 
    case LodV() => 
    case Lod() => 
    case Str() => 
    }
  }
}